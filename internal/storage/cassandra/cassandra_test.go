package cassandra

import (
	"testing"

	"github.com/bmizerany/assert"
)

func teardown() {
	if Cassandra != nil {
		Cassandra.Close()
	}
}

func TestInitCassandra(t *testing.T) {
	defer teardown()
	err := InitCassandra()
	t.Log(err)
	assert.Equal(t, nil, err)
}

func TestCreateDataSource(t *testing.T) {
	defer teardown()
	err := InitCassandra()
	assert.Equal(t, nil, err)
	err = CreateDataSource("sample_data_source_1")
	assert.Equal(t, nil, err)
	dss, err := GetAllDataSources()
	assert.NotEqual(t, 0, len(dss))
	for _, ds := range dss {
		t.Log(ds.Name)
	}
}

func TestCreateRawData(t *testing.T) {
	err := InitCassandra()
	assert.Equal(t, nil, err)
	defer teardown()
	err = CreateRawData("sample_data_source", []byte(`{"test": 2}`))
	assert.Equal(t, nil, err)
	data := GetLatestRawData("sample_data_source")
	t.Log(string(data))
}
