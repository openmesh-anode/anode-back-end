package cassandra

import (
    "errors"
    "fmt"

    "log"
    "time"

    "github.com/gocql/gocql"
    "gitlab.com/cloud-captains-of-unsw/anode/internal/model"
)

const (
    createDataSourceQuery   = "INSERT INTO anode.data_source(id, name) VALUES (uuid(), ?)"
    getAllDataSourceQuery   = "SELECT * FROM anode.data_source"
    getOneDataSourceByName  = "SELECT * FROM anode.data_source WHERE name = ? ALLOW FILTERING"
    createRawDataTableQuery = `CREATE TABLE IF NOT EXISTS anode.raw_%s (
	id UUID,
	raw_data BLOB,
    update_time TIMESTAMP,
    PRIMARY KEY (id),
);`
    createRawDataQuery  = "INSERT INTO anode.raw_%s(id, raw_data, update_time) VALUES (uuid(), ?, toTimestamp(now()))"
    getRawDataQuery     = "SELECT * FROM anode.raw_%s LIMIT 1"
    CreateInstanceQuery = "INSERT INTO anode.instance(id, ip, name, communication_port, http_port, ws_port) VALUES (uuid(),?, ?, ?, ?, ?)"
    getAllInstanceQuery = "SELECT * FROM anode.instance"
)

var Cassandra *gocql.Session

// InitCassandra initialises a Cassandra session
func InitCassandra() error {
    cluster := gocql.NewCluster("cassandra1")
    cluster.Port = 9042
    cluster.Keyspace = "anode"
    cluster.DisableInitialHostLookup = true
    cluster.ConnectTimeout = time.Second * 5
    sess, err := cluster.CreateSession()
    if err != nil {
        return err
    }
    Cassandra = sess
    log.Printf("Successfully connected to Cassandra: %s:%d.", "127.0.0.1", cluster.Port)
    return nil
}

// CreateDataSource creates a new data source with the specified name and its raw data table in Cassandra
func CreateDataSource(name string) error {
    ds, err := GetDataSourceByName(name)
    if err != nil && err.Error() == "Record not found" {
        if ds != nil {
            // Do nothing if name already exists in DB
            return errors.New("Record duplicated")
        }

        // Create data source itself
        if err := Cassandra.Query(createDataSourceQuery, name).Exec(); err != nil {
            return err
        }
        // Create the raw_data table
        if err := Cassandra.Query(fmt.Sprintf(createRawDataTableQuery, name)).Exec(); err != nil {
            return err
        }
        // Handle the error
        return nil
    }
    return errors.New("Record duplicated")
}

// CloseConnection closes the connection to Cassandra
func CloseConnection() {
    Cassandra.Close()
}

// GetAllDataSources returns all registered data sources
func GetAllDataSources() ([]model.DataSource, error) {
    ds := make([]model.DataSource, 0)

    iter := Cassandra.Query(getAllDataSourceQuery).Iter()
    for {
        m := map[string]interface{}{}
        if !iter.MapScan(m) {
            break
        }
        // dataSource := model.DataSource{
        // 	ID:   m["id"].(gocql.UUID).String(),
        // 	Name: m["name"].(string),

        // }
        id, ok := m["id"].(gocql.UUID)
        if !ok {
            // Handle error
            log.Printf("Error: wrong id format")
        }
        name, ok := m["name"].(string)
        if !ok {
            // Handle error
            log.Printf("Error: wrong name format")
        }
        dataSource := model.DataSource{
            ID:   id.String(),
            Name: name,
        }
        ds = append(ds, dataSource)
    }
    // Check for errors after the iteration
    if err := iter.Close(); err != nil {
        return nil, err
    }
    return ds, nil
}

// getDataSourceByName returns a single data source with the specified name
func GetDataSourceByName(name string) (*model.DataSource, error) {
    m := map[string]interface{}{}
    ds := &model.DataSource{}
    iter := Cassandra.Query(getOneDataSourceByName, name).Iter()
    for iter.MapScan(m) {
        ds.Name = m["name"].(string)
        ds.ID = m["id"].(gocql.UUID).String()
    }
    if ds.Name == "" && ds.ID == "" {
        return nil, errors.New("Record not found")
    }
    return ds, nil
}

// ========== Raw data functions ==========

// CreateRawData insert the raw data fetched into Cassandra
func CreateRawData(name string, data []byte) error {
    return Cassandra.Query(fmt.Sprintf(createRawDataQuery, name), data).Exec()
}

// GetLatestRawData return the last raw data fetched from Cassandra
func GetLatestRawData(name string) []byte {
    m := map[string]interface{}{}
    data := make([]byte, 0)
    iter := Cassandra.Query(fmt.Sprintf(getRawDataQuery, name)).Iter()
    for iter.MapScan(m) {
        data = m["raw_data"].([]byte)
    }
    return data
}

// create instance with ip, communication_port, http_port, ws_port
func CreateInstance(ip string, name string, communication_port string, http_port string, ws_port string) error {
    return Cassandra.Query(CreateInstanceQuery, ip, name, communication_port, http_port, ws_port).Exec()
}

// get all instances
func GetAllInstances() ([]model.Instance, error) {
    instances := make([]model.Instance, 0)
    iter := Cassandra.Query(getAllInstanceQuery).Iter()

    for {
        m := map[string]interface{}{}
        if !iter.MapScan(m) {
            break
        }
        ip, ok := m["ip"].(string)
        if !ok {
            // Handle error
            log.Printf("Error: wrong ip format")
        }
        communication_port, ok := m["communication_port"].(string)
        if !ok {
            // Handle error
            log.Printf("Error: wrong communication_port format")
        }
        http_port, ok := m["http_port"].(string)
        if !ok {
            // Handle error
            log.Printf("Error: wrong http_port format")
        }
        ws_port, ok := m["ws_port"].(string)
        if !ok {
            // Handle error
            log.Printf("Error: wrong ws_port format")
        }
        instance := model.Instance{
            IP:                ip,
            Name:              m["name"].(string),
            CommunicationPort: communication_port,
            HTTPPort:          http_port,
            WsPort:            ws_port,
            Alive:             true,
        }
        instances = append(instances, instance)
    }
    // Check for errors after the iteration
    if err := iter.Close(); err != nil {
        return nil, err
    }
    return instances, nil
}
