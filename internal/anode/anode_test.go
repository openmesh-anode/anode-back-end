package anode

import (
	"github.com/bmizerany/assert"
	"testing"
)

func TestInitANode(t *testing.T) {
	node, err := InitANode(9090, 9091, 9092, "ANode-1")
	assert.Equal(t, nil, err)
	defer node.Leave()
	node1, err := InitANode(9093, 9094, 9095, "ANode-2")
	assert.Equal(t, nil, err)
	defer node1.Leave()
	node.Join([]string{"127.0.0.1:9093"})
}

func TestANode_HealthCheck(t *testing.T) {
	node, err := InitANode(9090, 9091, 9092, "ANode-1")
	assert.Equal(t, nil, err)
	defer node.Leave()
	node1, err := InitANode(9093, 9094, 9095, "ANode-2")
	assert.Equal(t, nil, err)
	defer node1.Leave()
	node.Join([]string{"127.0.0.1:9093"})
	instances := node1.HealthCheck()
	for i, ins := range instances {
		t.Logf("Instance %d: %v", i, ins)
	}
}
