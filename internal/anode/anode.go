package anode

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"time"

	"github.com/hashicorp/memberlist"
	"gitlab.com/cloud-captains-of-unsw/anode/internal/model"
)

var (
	Instance *ANode
)

// ANode is a aNode instance
type ANode struct {
	Name          string
	GossipPort    int
	HTTPPort      int
	WebSocketPort int

	MsgChan    chan InterNodeMsg
	NotifyChan chan struct{}
	ML         *memberlist.Memberlist
	Server     *http.Server
}

// InitANode initialise a standalone aNode instance
func InitANode(gossipPort, httpPort int, name string) error {
	instance := &ANode{
		GossipPort: gossipPort,
		HTTPPort:   httpPort,
		Name:       name,
		// NotifyChan is an unbuffered channel that will be filled when receiving a new message
		NotifyChan: make(chan struct{}),
		MsgChan:    make(chan InterNodeMsg, 10), // MsgChan is a buffered channel
	}

	// Initialise a memberlist.List for this instance
	conf := memberlist.DefaultLocalConfig()
	conf.BindPort = gossipPort
	conf.Name = name
	conf.Delegate = &DataDelegate{}
	list, err := memberlist.Create(conf)
	if err != nil {
		return err
	}
	instance.ML = list

	// Initialise HTTP server
	instance.Server = &http.Server{Addr: fmt.Sprintf(":%d", httpPort)}
	// WebSocket APIs
	http.HandleFunc("/publish", Publish)
	http.HandleFunc("/subscribe", Subscribe)
	Instance = instance

	return nil
}

// ServeHTTP start the HTTP and WebSocket server of this aNode
func (a *ANode) ServeHTTP() {
	err := a.Server.ListenAndServe()
	if err != nil {
		log.Fatalf("Failed to start http server: %s", err.Error())
	}
}

// Join joins this aNode instance to a cluster
func (a *ANode) Join(entry []string) {
	if len(entry) <= 0 {
		// len(entry) == 0 means this aNode instances is standalone
		return
	}
	t := time.NewTimer(time.Second * 5)
JoinLoop:
	for {
		select {
		case <-t.C:
			// Try to join a cluster via a specified entrypoint
			_, err := a.ML.Join(entry)
			if err != nil {
				log.Printf("Failed to join the cluster. Error: %s", err.Error())
				continue
			}
			// Successfully joined a cluster
			log.Printf("Successfully joined the cluster via %v.", entry)
			break JoinLoop
		}
	}
	return
}

// Leave remove this aNode from the cluster joined
func (a *ANode) Leave() error {
	if err := a.ML.Leave(5 * time.Second); err != nil {
		return err
	}
	log.Printf("Successfully left the cluster.")
	return nil
}

// HealthCheck do a health check and return list for the known members with known nodes when called
func (a *ANode) HealthCheck() []model.Instance {
	knownInstances := make([]model.Instance, 0)
	for _, member := range a.ML.Members() {
		hostname := member.Addr.String()
		port := member.Port
		name := member.Name
		timeout := 5 * time.Second

		// TODO specify the HTTPPort and WebSocketPort by meta
		conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", hostname, port), timeout)
		if err != nil {
			// This instance is dead
			knownInstances = append(knownInstances, model.Instance{
				IP:                hostname,
				Name:              name,
				CommunicationPort: strconv.Itoa(int(port)),
				HTTPPort:          strconv.Itoa(a.HTTPPort),
				Alive:             false,
			})
		} else {
			// This instance is alive
			knownInstances = append(knownInstances, model.Instance{
				IP:                hostname,
				Name:              name,
				CommunicationPort: strconv.Itoa(int(port)),
				HTTPPort:          strconv.Itoa(a.HTTPPort),
				Alive:             true,
			})
		}

		if conn != nil {
			if err := conn.Close(); err != nil {
				log.Printf("Unable to close connection after health check: %s", err.Error())
			}
		}
	}
	return knownInstances
}
