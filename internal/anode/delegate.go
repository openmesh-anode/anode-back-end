package anode

import (
	"encoding/json"
	"github.com/hashicorp/memberlist"
	"log"
	"sync"
)

var (
	NewestMsgs      map[string]InterNodeMsg
	DataBroadcaster *memberlist.TransmitLimitedQueue
	lock            sync.Mutex
)

// InterNodeMsg is a single message that passed by nodes via gossip
type InterNodeMsg struct {
	Name      string `json:"name"`
	Msg       []byte `json:"msg"`
	Timestamp int64  `json:"timestamp"`
}

// DataDelegate completes the synchronisation for the newest data in the cluster
type DataDelegate struct{}

// DataBroadcast broadcasts the newest data
type DataBroadcast struct {
	Msg []byte
}

// InitBroadcaster initialises the broadcaster using the memberlist
func InitBroadcaster() {
	NewestMsgs = make(map[string]InterNodeMsg)
	DataBroadcaster = &memberlist.TransmitLimitedQueue{
		NumNodes: func() int {
			return Instance.ML.NumMembers()
		},
		RetransmitMult: 5,
	}
}

func (b *DataBroadcast) Invalidates(br memberlist.Broadcast) bool {
	return false
}

func (b *DataBroadcast) Message() []byte {
	return b.Msg
}

func (b *DataBroadcast) Finished() {
	return
}

func (d *DataDelegate) NodeMeta(limit int) []byte {
	return []byte{}
}

// NotifyMsg will be called when receiving a new message through broadcast
func (d *DataDelegate) NotifyMsg(msg []byte) {
	if len(msg) == 0 {
		return
	}
	m := InterNodeMsg{}
	if err := json.Unmarshal(msg, &m); err != nil {
		log.Printf("Failed to marshal received message %s, err: %s", string(msg), err.Error())
		return
	}
	lock.Lock()
	defer lock.Unlock()

	// Send this node to websocket apis if this message is newer
	if m.Timestamp > NewestMsgs[m.Name].Timestamp {
		Instance.MsgChan <- m
		Instance.NotifyChan <- struct{}{}
	}
	// Update local state
	NewestMsgs[m.Name] = m
}

func (d *DataDelegate) GetBroadcasts(overhead, limit int) [][]byte {
	return DataBroadcaster.GetBroadcasts(overhead, limit)
}

// LocalState marshal local state to json and provide it via gossip
func (d *DataDelegate) LocalState(join bool) []byte {
	lock.Lock()
	m := NewestMsgs
	lock.Unlock()
	ls, err := json.Marshal(m)
	if err != nil {
		return []byte{}
	}
	return ls
}

// MergeRemoteState use the remote state to update the local state
func (d *DataDelegate) MergeRemoteState(rs []byte, join bool) {
	if len(rs) == 0 || !join {
		return
	}
	remoteState := make(map[string]InterNodeMsg)
	if err := json.Unmarshal(rs, &remoteState); err != nil {
		log.Printf("Failed to unmarshal remote state %s, error: %s", string(rs), err.Error())
		return
	}
	lock.Lock()
	defer lock.Unlock()
	for k, v := range remoteState {
		if _, ok := NewestMsgs[k]; !ok {
			// Send this message to websocket if it's not exist in local state
			Instance.MsgChan <- InterNodeMsg{
				Name: k,
				Msg:  v.Msg,
			}
			Instance.NotifyChan <- struct{}{}
		} else if NewestMsgs[k].Timestamp < v.Timestamp {
			// Or it's exist, but newer
			Instance.MsgChan <- InterNodeMsg{
				Name: k,
				Msg:  v.Msg,
			}
			Instance.NotifyChan <- struct{}{}
		}
		NewestMsgs[k] = v
	}
}

func GetNewestMsgs(name string) (InterNodeMsg, bool) {
    lock.Lock()
    defer lock.Unlock()
    data, exists := NewestMsgs[name]
    return data, exists
}