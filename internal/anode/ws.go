package anode

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"gitlab.com/cloud-captains-of-unsw/anode/internal/storage/cassandra"
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"
)

var (
	upgrader = &websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	h       *hub
	hubLock sync.Mutex
)

type hub struct {
	// Key: name of the data source
	// Value: connections
	subscribers map[string][]*connection
}

type connection struct {
	ws  *websocket.Conn // WebSocket connection
	buf chan []byte     // Buffer for read and write
}

// InitHub initialise a new hub and set it to global variable h
func InitHub() {
	h = &hub{subscribers: make(map[string][]*connection)}
}

// Publish is the WebSocket API that push data into the system
func Publish(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("Failed to upgrade: %s", err.Error())
		return
	}
	defer ws.Close()
	c := &connection{
		ws:  ws,
		buf: make(chan []byte, 256),
	}
	name, err := url.QueryUnescape(r.FormValue("name"))
	if err != nil {
		// Use the escaped name if failed to unescape
		name = r.FormValue("name")
	}
	c.readPublisher(name)
}

// readPublisher read from a WebSocket connection and store the data to database
func (c *connection) readPublisher(name string) {
	defer func() {
		c.ws.Close()
	}()
	c.ws.SetPongHandler(func(string) error { return c.ws.SetReadDeadline(time.Now().Add(60 * time.Second)) })
	for {
		_, msg, err := c.ws.ReadMessage()
		if err != nil {
			log.Printf("Failed to read from WebSocket connection: %s", err.Error())
			break
		}

		// Save this message to cassandra
		err = cassandra.CreateRawData(name, msg)
		if err != nil {
			log.Printf("Failed to store data to Cassandra: %s", err.Error())
		}

		// Broadcast to local subscribers
		for _, conn := range h.subscribers[name] {
			conn.buf <- msg
		}
		// TODO Broadcast to other aNodes subscribers via Gossip
		broadcastMsg := &InterNodeMsg{
			Name:      name,
			Msg:       msg,
			Timestamp: time.Now().Unix(),
		}
		NewestMsgs[name] = *broadcastMsg
		bm, _ := json.Marshal(broadcastMsg)
		DataBroadcaster.QueueBroadcast(&DataBroadcast{
			Msg: bm,
		})

		// Write initial response
		err = c.ws.WriteMessage(websocket.TextMessage, []byte(`{"ok": true}`))
		if err != nil {
			log.Printf("Failed to write initial response: %s", err.Error())
			break
		}
	}
}

// Subscribe is the WebSocket API that push fetched data into
func Subscribe(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("Failed to upgrade: %s", err.Error())
		return
	}
	defer ws.Close()

	// Register this subscriber
	name, err := url.QueryUnescape(r.FormValue("name"))
	if err != nil {
		// Use the escaped name if failed to unescape
		name = r.FormValue("name")
	}
	c := &connection{
		ws:  ws,
		buf: make(chan []byte, 256),
	}
	hubLock.Lock()
	// Add this subscriber to hub
	h.subscribers[name] = append(h.subscribers[name], c)
	hubLock.Unlock()
	c.writeSubscriber()
}

// writeSubscriber write the fetched data to subscribers
func (c *connection) writeSubscriber() {
	for {
		select {
		case msg := <-c.buf:
			if err := c.ws.WriteMessage(websocket.BinaryMessage, msg); err != nil {
				log.Printf("Failed to write to subscriber: %s", err.Error())
				return
			}
		case <-Instance.NotifyChan:
			im := <-Instance.MsgChan
			// Send this message to all subscribers
			for _, subscriber := range h.subscribers[im.Name] {
				subscriber.buf <- im.Msg
			}
		}
	}
}
