package model

type Instance struct {
	IP                string `json:"ip"`
	Name              string `json:"name"`
	CommunicationPort string `json:"communication_port"`
	HTTPPort          string `json:"http_port"`
	WsPort            string `json:"ws_port"`
	Alive             bool   `json:"alive"`
}
