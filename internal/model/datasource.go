package model

// DataSource is a single data source that registered to aNode
type DataSource struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
