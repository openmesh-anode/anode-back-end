package model

import "time"

type RawData struct {
	ID         string    `json:"id"`
	RawData    []byte    `json:"raw_data"`
	UpdateTime time.Time `json:"update_time"`
}
