package main

import (
    "fmt"
    "log"
    "net/http"
    "os"
    "strconv"
    "strings"

    "github.com/gin-gonic/gin"
    "gitlab.com/cloud-captains-of-unsw/anode/internal/anode"
    "gitlab.com/cloud-captains-of-unsw/anode/internal/storage/cassandra"
)

func main() {
    // Initialise Cassandra
    err := cassandra.InitCassandra()
    if err != nil {
        panic(err)
    }

    router := gin.Default()

    router.GET("/api/ds", func(c *gin.Context) {
        // Call your GetAllDataSources function here.
        // For example:
        dataSources, err := cassandra.GetAllDataSources()
        if err != nil {
            c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
            return
        } else {
            fmt.Println("data retrieved")
            // c.JSON(http.StatusOK, dataSources)
            c.JSON(http.StatusOK,
                gin.H{

                    "data": gin.H{
                        "data_sources": dataSources,
                    },
                    "current":  0,
                    "pageSize": 0,
                    "total":    0,
                })
        }

    })

    router.GET("/api/ds/:name", func(c *gin.Context) {
        // Call your GetDataSourceByName function here.
        // For example:
        name := c.Param("name")
        dataSource, err := cassandra.GetDataSourceByName(name)
        if err != nil {
            c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
            return
        }
        c.JSON(http.StatusOK, dataSource)
    })

    router.GET("/api/latest-raw-data-sources/:name", func(c *gin.Context) {
        // Call your GetDataSourceByName function here.
        // For example:
        name := c.Param("name")
        dataSource := cassandra.GetLatestRawData(name)
        if len(dataSource) == 0 {
            c.JSON(http.StatusInternalServerError, gin.H{"error": "Data source not found"})
            return
        }
        c.JSON(http.StatusOK, dataSource)
    })

    router.GET("/api/newest-messages/:name", func(c *gin.Context) {
        name := c.Param("name")
        data, exists := anode.GetNewestMsgs(name)
        if !exists {
            c.JSON(http.StatusNotFound, gin.H{"error": "Data source not found"})
            return
        }
        c.JSON(http.StatusOK, data)
    })

    //router get all instances
    router.GET("/api/instances", func(c *gin.Context) {
        // Call your GetAllDataSources function here.
        // For example:
        instances := anode.Instance.HealthCheck()
        c.JSON(http.StatusOK, gin.H{
            "code": 0,
            "msg":  "",
            "instances": gin.H{
                "instances": instances,
            },
        })
    })

    router.POST("/api/ds", func(c *gin.Context) {
        var json struct {
            Name string `json:"name"`
        }
        if err := c.BindJSON(&json); err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        err := cassandra.CreateDataSource(json.Name)
        if err != nil {
            c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
            return
        }
        // c.JSON(http.StatusOK, gin.H{"status": "success"})
        result, error := cassandra.GetAllDataSources()
        if error != nil {
            c.JSON(http.StatusInternalServerError, gin.H{"error": error.Error()})
            return
        }
        c.JSON(http.StatusOK,
            gin.H{
                "Lists": gin.H{
                    "data_source": result,
                    "desc":        "test",
                },
                "Pagination": gin.H{
                    "total": 1,
                },
            })

    })

    router.POST("/api/create-raw-data", func(c *gin.Context) {
        // Call your GetAllDataSources function here.
        var json struct {
            Name string `json:"name"`
            Data []byte `json:"data"`
        }
        if err := c.BindJSON(&json); err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        err := cassandra.CreateRawData(json.Name, json.Data)
        if err != nil {
            c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
            return
        }
        c.JSON(http.StatusOK, gin.H{"status": "success"})
    })

    router.POST("/api/create-instance", func(c *gin.Context) {
        // Call your GetAllDataSources function here.
        var json struct {
            Ip                 string `json:"ip"`
            Name               string `json:"name"`
            Communication_port string `json:"communication_port"`
            Http_port          string `json:"http_port"`
            Ws_port            string `json:"ws_port"`
        }
        if err := c.BindJSON(&json); err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        err := cassandra.CreateInstance(json.Ip, json.Name, json.Communication_port, json.Http_port, json.Ws_port)
        if err != nil {
            c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
            return
        }
        c.JSON(http.StatusOK, gin.H{"status": "success"})
    })

    // Initialise hub
    anode.InitHub()
    // Initialise aNode gossip cluster: initialise instance and broadcaster
    nodeName := os.Getenv("ANODE_NAME")
    if nodeName == "" {
        nodeName = "aNode-1"
    }
    gossipPort, _ := strconv.Atoi(os.Getenv("GOSSIP_PORT"))
    if gossipPort == 0 {
        gossipPort = 9090
    }
    wsPort, _ := strconv.Atoi(os.Getenv("WS_PORT"))
    if wsPort == 0 {
        wsPort = 8080
    }
    httpPort, _ := strconv.Atoi(os.Getenv("HTTP_PORT"))
    if httpPort == 0 {
        httpPort = 3000
    }
    knownNodes := os.Getenv("KNOWN_NODES")
    err = anode.InitANode(gossipPort, wsPort, nodeName)
    if err != nil {
        panic(err)
    }
    anode.InitBroadcaster()
    if len(knownNodes) > 0 {
        anode.Instance.Join(strings.Split(knownNodes, ","))
    }

    // Start gossip cluster
    go func() {
        anode.Instance.ServeHTTP()
    }()

    log.Printf("aNode %s is listening on port %d for WebSocket and port %d for gossip.", nodeName, wsPort, gossipPort)
    router.Run(fmt.Sprintf(":%d", httpPort))
}
