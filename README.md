# aNode Back-end Overview

## Important Notice

This repository was archived due to the end of the hackathon. For future development and document, please refer to: [https://github.com/Project-aNode](https://github.com/Project-aNode)

## Introduction

aNode is a solution to the OpenD/I Hackathon problem 3. This is the repository for the back-end of the aNode project.

`aNode` means "a node": every node is the same, which indicates that this system is completely decentralized and P2P.

## Overview

The problem is to build a prototype of a Web3 infrastructure that can subscribe to over 100 data sources, pull from those data sources, and transfer data to RESTful APIs and WebSockets in near real-time. This system should be high-quality and low-cost, and it should also be stateless and easy to set up.

## Requirements

### Functional Requirements

- Able to add a data source to the system.
- Able to subscribe a data source.
- Pull the newest data from data source.
- Get the newest data from the system through either RESTful APIs or WebSockets.
- Provide the status of the system for the front-end through RESTful APIs.
- Distributed, decentralized is better, but this is not a technical requirement.
- Microservices should be stateless to support automatic scaling.
- Push: pushing fetched data to WebSocket servers.
- Pull: allow users get the newest data (or the first data that has not been fetched) through RESTful APIs, WebSockets.
- Supports GraphQL.

### Non-functional Requirements

- Able to handle more than 100 data sources.
- Provide Docker images and Dockerfiles.
- Easy to use, maintain and deploy.
- High-performance and low-cost.
- Transfer data in nearly real-time.
- Automatically scale-up and scale-down (optional).
- Uncentralized, meets the requirement of Web3 dApps (optional).

## Technical Process

Following would be the languages and tools we would use to develop this application:

- Back-end development: Go
- Database: Apache Cassandra
- Deployment: Docker

## Table Definitions (WIP)

TODO: Table definitions based on the data model of Apache Cassandra.

## Architecture & Design

### Architecture

Architecture of aNode:

![](https://cdn.jsdelivr.net/gh/classmateada/site-pictures/img/20231212102946.png)

Taking advanteage of Apache Cassandra, a decentralized distributed database, aNode does not worry about synchronization of the data through the decentralized network. Besides, aNode itself is also completely decentralized and P2P like Apache Cassandra. It 's based on "aProtocol", a modified version of the Gossip protocol, and complies with the **AP** model in the CAP theorem.

### Events (WIP)

aNode is event-driven, which means every kind of changes (e.g., register, health check, unregister) are represented by events. aNode instances will regularly exchange those events with other instances.

### Interact with Data Sources (Publishers)

aNode can receive pushed data from data sources through WebSocket. It can also pull data periodically by a custom period specified by the user through RESTful APIs.

1. **Push.** Data sources are WebSocket clients, and they will push their data when they need to do so.
2. **Pull.** Data sources are RESTful API providers. We should check their APIs periodically for any data updates.

### Interact with Subscribers

aNode can either push or pull data to subscribers:

1. **Push (default).** Based on WebSocket.
2. **Pull.** Based on RESTful APIs.

In the pull mechanism, there're two different mode which returns different kind of data:

1. **Newest only (NO) (default).** Always return the newest data fetched from the data source. That's what Xnode does.
   1. Use case: game leaderboard.
2. **First unread (FU).** Returns the first unread data.
   1. Use case: decentralized research platform.

## Future Works

1. **Use decentralized storage (like Tableland).** We are now using Apache Cassandra to store every kinds of data, which is distributed but not fully decentralized. Apache Cassandra copies all data to all nodes, which is not effective and leads to storage waste.

## Mentoring Session Conclusion

1. AP is better than CP, but it's depend on the use case.
2. The data is sent by WebSocket by push, but some of them will be pull. Off-chain data have off-chain WebSocket, on-chain data may be pull. Don't need to worry about that, that should be push.
3. Always return the newest data.
4. Whether copy everything to every node is depend on me. Apache Cassandra is fine.
